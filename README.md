## Musical hearing simulator

Simple simulator for training recognizing different guitar and piano notes.

### Libraries used
- sqlite3 (for data base)
- playsound (for sounds)
- PyQt5 (for GUI)

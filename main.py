import sqlite3
import sys
import random
from playsound import playsound
from PyQt5 import uic
from PyQt5.QtWidgets import *
from PyQt5 import QtGui


class MyProject(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('main.ui', self)
        self.connection = sqlite3.connect("db_project.sqlite")
        self.cur = self.connection.cursor()
        self.setWindowTitle('Тренажер музыкального слуха')
        self.setWindowIcon(QtGui.QIcon('music.ico'))
        self.begin.clicked.connect(self.run)
        self.sound.clicked.connect(self.play)
        self.v1.clicked.connect(self.ans)
        self.v2.clicked.connect(self.ans)
        self.v3.clicked.connect(self.ans)
        self.v4.clicked.connect(self.ans)
        self.cont.clicked.connect(self.contin)
        self.cont.hide()

        self.v1.setStyleSheet('QPushButton {background-color: #FFA500}')
        self.v2.setStyleSheet('QPushButton {background-color: #FFA500}')
        self.v3.setStyleSheet('QPushButton {background-color: #FFA500}')
        self.v4.setStyleSheet('QPushButton {background-color: #FFA500}')
        # Флаги для определения, какой инструмент выбрал пользователь
        self.piano_sounds = False
        self.guitar_sounds = False
        # Баллы, которые может набирать пользователь
        self.ans_sheet = 0
        # Номер задания
        self.num_task = 1

    def run(self):
        if not self.piano.isChecked() and not self.guitar.isChecked():
            # Сообщение об ошибке, если пользователь не выбрал инструмент
            QMessageBox.critical(self, "Ошибка ", "Инструмент не выбран", QMessageBox.Ok)
        else:
            # Установка текста на вариантах ответов (радиокнопках)
            if self.piano.isChecked():
                self.question.setText('Какой аккорд прозвучал?')
                self.piano_sounds = True
                self.v1.setText('C')
                self.v2.setText('Cm')
                self.v3.setText('Csus2')
                self.v4.setText('Csus4')
            else:
                self.question.setText('Какая струна прозвучала?')
                self.guitar_sounds = True
                self.v1.setText('1 струна')
                self.v2.setText('3 струна')
                self.v3.setText('4 струна')
                self.v4.setText('6 струна')
            # Установка неактивной первой части приложения
            self.begin.setEnabled(False)
            self.piano.setEnabled(False)
            self.guitar.setEnabled(False)
            self.instrument.setEnabled(False)

            # Установка активной тестовой части приложения
            self.task.setEnabled(True)
            self.question.setEnabled(True)
            self.sound.setEnabled(True)

            self.v1.setEnabled(True)
            self.v2.setEnabled(True)
            self.v3.setEnabled(True)
            self.v4.setEnabled(True)

            # Базы данных
            # if self.guitar_sounds:
            #     self.sound_list = self.cur.execute("SELECT sound_name FROM guitar").fetchall()
            # elif self.piano_sounds:
            #     self.sound_list = self.cur.execute("SELECT sound_name FROM piano").fetchall()
            # Базы данных

            # Списки
            if self.piano_sounds:
                self.sound_list = ['C.mp3',
                                   'Cm.mp3',
                                   'Csus2.mp3',
                                   'Csus4.mp3']
            elif self.guitar_sounds:
                self.sound_list = ['1_struna.mp3',
                                   '3_struna.mp3',
                                   '4_struna.mp3',
                                   '6_struna.mp3']
            # Списки
            self.generate_id()

    def generate_id(self):
        # Генерация мелодии
        self.sound_id = random.choice(self.sound_list)
        self.sound_list.remove(self.sound_id)

    def play(self):
        # Проигрывание звука
        playsound(self.sound_id)
        # Следующая строка нужна только для проверки правильности работы программы и служит для подсказки о мелодии,
        # которая сейчас играет
        print(self.sound_id)

    def contin(self):
        # Смена цвета выбрнной кнопки на первоначальный
        self.now_button.setStyleSheet('QPushButton {background-color: #FFA500}')
        self.v1.setEnabled(True)
        self.v2.setEnabled(True)
        self.v3.setEnabled(True)
        self.v4.setEnabled(True)
        # Смена на следующее задание
        self.num_task += 1
        # Если пользователь закончил третье задание, печатается результат
        if self.num_task > 3:
            self.create_result()
        # Если нет, то выбирается новый звук, и меняется номер задания
        else:
            self.task.setText('Задание №' + str(self.num_task))
            self.generate_id()
        self.cont.hide()

    def ans(self):
        # Запись выбранного ответа
        self.now_button = self.sender()
        pers_ans = self.sender().text()

        # Проверка на правильный ответ
        if self.guitar_sounds:
            if self.sound_id[0] == pers_ans[0]:
                self.sender().setStyleSheet('QPushButton {background-color: #9ACD32}')
                self.ans_sheet += 1
            else:
                self.sender().setStyleSheet('QPushButton {background-color: #FF0000}')
        else:
            if self.sound_id[:-4] == pers_ans:
                self.sender().setStyleSheet('QPushButton {background-color: #9ACD32}')
                self.ans_sheet += 1
            else:
                self.sender().setStyleSheet('QPushButton {background-color: #FF0000}')
        self.v1.setEnabled(False)
        self.v2.setEnabled(False)
        self.v3.setEnabled(False)
        self.v4.setEnabled(False)
        if self.num_task == 3:
            self.cont.setText('Завершить тестирование')
        self.cont.show()

    def create_result(self):
        # Создание текстового документа с результатом и оценкой
        with open('result.txt', 'wt', encoding='utf-8') as print_res:
            print_res.write('----------Ваш результат----------\n')
            if self.piano_sounds:
                instr = ' фортепиано'
            else:
                instr = ' гитара'
            print_res.write('Вы прошли тест на музыкальный слух на инструменте' + instr + '.\n')
            if self.ans_sheet == 0:
                task_name = ' заданий'
            elif self.ans_sheet == 1:
                task_name = ' задание'
            else:
                task_name = ' задания'
            print_res.write('Вы решили правильно ' + str(self.ans_sheet) + task_name + '.\n')
            mark = 2
            # Вычисление оценки
            if self.ans_sheet == 3:
                mark = 5
            elif self.ans_sheet == 2:
                mark = 4
            elif self.ans_sheet == 1:
                mark = 3
            print_res.write('Ваша оценка: ' + str(mark))
        self.close()

    # Отключение базы данных
    def closeEvent(self, event):
        self.connection.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MyProject()
    ex.show()
    sys.exit(app.exec())
